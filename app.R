

library(shiny)

#Define UI for slider demo app
ui <- fluidPage(
  
  # App title 
  titlePanel("Predicting probability of having any complication"),
  #Add radio buttons
  radioButtons("diabetes", "Diabetes (yes =1, no=0)",
               list("1",
                    "0")),
  
  radioButtons("chemo_after", "Post-op Chemotherapy (yes =1, no=0)",
               list("1",
                    "0")),
  radioButtons("radiation_before", "Pre-op Radiation (yes =1, no=0)",
               list("1",
                    "0")),
  radioButtons("plane", "Plane of reconstruction",
               list("DP",
                    "PP")),
  # Sidebar layout with input and output definitions 
  sidebarLayout(
    
    # Sidebar to demonstrate various slider options
    sidebarPanel(
      
      # Input: Simple integer interval
      sliderInput("age", "Age",
                  min = 0, max = 100,
                  value = 50)
    ),
      
    # Main panel for displaying outputs
    mainPanel(
      
      # Output: Table summarizing the values entered
      tableOutput("values")
    )
  )
)


# Define server logic for slider examples 
server <- function(input,output) {
  load('bmiLogisticRegression.rda') #name of model is step.model
  Values <- reactive({
    df_in<- data.frame(diabetes = input$diabetes, chemo_after = input$chemo_after, age = input$age, radiation_before = input$radiation_before, plane = input$plane)
    prob <- predict(step.model, df_in, type = "response")
  })
  
  
  
  
  # Show the values in an HTML table
  output$values <- renderTable({
    Values()
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)